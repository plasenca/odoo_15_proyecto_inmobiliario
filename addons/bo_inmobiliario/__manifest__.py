{
    "name":"Seguimiento de Inmuebles",
    "version":"1.0.0",
    "depends":["base"],
    "demo":[
        "demo/demo.xml"
    ],
    "data":[
        "security/groups.xml",
        "views/views.xml"
    ],
    "author":"Daniel Moreno",
    "description":"""
        Este módulo permitirá registrar inmuebles y etiquetarlos. 
        Así como también registrar el estado en el que se encuentran,
        como por ejemplo si se encuentra disponible, en remodelación,
        en negociación o vendido.
    """
}


