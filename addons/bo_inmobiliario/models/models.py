from odoo import models, fields

class BOTags(models.Model):
    _name = "bo.tag"
    _description = "Etiquetas"

    name = fields.Char(string="Nombre")

class BOInmueble(models.Model):
    _name = "bo.inmueble"
    _description = "Inmuebles"

    name = fields.Char(string="Nombre",required="1")
    street = fields.Char(string="Dirección")
    value = fields.Float(string="Valor",default=0)
    type = fields.Selection(selection=[("departamento","Departamento"),
                                        ("casa","Casa"),
                                        ("terreno","Terreno")],string="Tipo de Inmueble",default="departamento",required="1")
    
    tag_ids = fields.Many2many("bo.tag",string="Etiquetas")
    partner_ids = fields.Many2many("res.partner",string="Interesados")


    currency_id = fields.Many2one("res.currency",string="Moneda",required="1",default=lambda r:r.env.company.currency_id)
    image = fields.Binary("Imagen referencial")
    area = fields.Float("Área m2")
    rooms = fields.Integer("Habitaciones")
    bath = fields.Integer("Baños")
    department_number = fields.Char("Número de departamento")


"""
El modelo res.partner, ya existe en Odoo, y lo usaremos para registrar los interesados de un inmueble.
"""
class ResParter(models.Model):
    _inherit = "res.partner"

    inmueble_ids = fields.Many2many("bo.inmueble",string="Inmuebles")


